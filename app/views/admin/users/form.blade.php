@extends ('admin/layout')

<?php
    if ($user->exists):
        $form_data = array('route' => array('admin.users.update', $user->id), 'method' => 'PATCH');
        $action    = 'Editar';
    else:
        $form_data = array('route' => 'admin.users.store', 'method' => 'POST');
        $action    = 'Nuevo';
    endif;
?>


@section ('title')
    {{ $action }} Usuario
@stop


@section ('content')
    <h1 class="text-center">{{ $action }} Usuario</h1>

    {{ Form::model($user, $form_data, array('role' => 'form')) }}
        @include ('admin/errors', array('errors' => $errors))

        <div class="row">
            <div class="form-group col-md-6">
                {{ Form::label('email', 'Dirección de E-mail') }}
                {{ Form::text('email', null, array('placeholder' => 'Introduce tu E-mail', 'class' => 'form-control')) }}
            </div>
            <div class="form-group col-md-6">
                {{ Form::label('full_name', 'Nombre completo') }}
                {{ Form::text('full_name', null, array('placeholder' => 'Introduce tu nombre y apellido', 'class' => 'form-control')) }}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                {{ Form::label('password', 'Contraseña') }}
                {{ Form::password('password', array('class' => 'form-control')) }}
            </div>
            <div class="form-group col-md-6">
                {{ Form::label('password_confirmation', 'Confirmar contraseña') }}
                {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="pull-right">
            <a href="{{ route('admin.users.index') }}" class="btn btn-default">Cancelar</a>


            {{ Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}
        </div>

    {{ Form::close() }}

@stop