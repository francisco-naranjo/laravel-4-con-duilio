@extends ('admin/layout')


@section ('title')
    Usuario {{ $user->full_name }}
@stop


@section ('content')
    <h1 class="text-center">Detalle usuario</h1>
    <p class="pull-right">
        <a href="{{ route('admin.users.index') }}" class="btn btn-default">Regresar</a>
        <a href="{{ route('admin.users.create') }}" class="btn btn-primary">Nuevo usuario</a>
    </p>

    <table class="table table-striped table-bordered">

        <tr class="info">
            <th>Full name</th>
            <th>Email</th>
            <th>created_at</th>
        </tr>

        <tr>
            <td>{{ $user->full_name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->created_at }}</td>
        </tr>

    </table>

@stop
