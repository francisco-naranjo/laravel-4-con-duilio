{{-- views/layout.blade.php --}}
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>@yield('title', 'Aprendiendo Laravel')</title>
    {{ HTML::script('assets/js/admin.js') }}
</head>
<body>
    @yield('content')
    <hr />
    Copyright 2013 - Todos los ponies reservados.
</body>
</html>